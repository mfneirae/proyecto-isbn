window.onload = getSession();

async function getSession() {
    const url = `/api/session/id_user`;
    const opciones = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }  
    try {
      const respuesta = await fetch(url, opciones);
      const datosRespuesta = await respuesta.json();
      sessionStorage.setItem("id_user",datosRespuesta)
    } catch (error) {
      console.error(error);
    }
  } 