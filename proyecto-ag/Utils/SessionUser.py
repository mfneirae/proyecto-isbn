from flask import session


class SessionUser:

    @staticmethod
    def create_session_user(data, key):
        session[key] = data
        return 1
    
    @staticmethod
    def get_key_session_user(key):
        data = ""
        if dict(session).get(key): 
            data = dict(session)[key]              
        return data
    