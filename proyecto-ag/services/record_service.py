from consumers.contabilidad_ms.contabilidad_consumer import *
from consumers.categorias_ms.categorias_consumer import *


class RecordService:

    @staticmethod
    def create_record_service(data):

        spei_data = {
            "id_user": data['token_id'],
            "id_category": data['category_id'],
            "type": data['type'],
            "amount": data['amount']
        }

        spei = create_spei(spei_data)

        if spei.status_code == 201:

            category_data = {
                "id_category": data['category_id'],
                "details": {
                    "id_user": data['token_id'],
                    "category_name": data['task_name'],
                    "category_type": data['type'],
                    "description": data['task_description']
                }
            }

            category = create_category(category_data)

            if category.status_code == 201:

                return True

        return False
