from repositories.user_repository import UserRepository


class UserService:
    
    @staticmethod
    def get_user_service(data):
        email = data['email']
        return UserRepository.get_user_repository(email)
