import requests

from config import USERS_MS_API_URL

base_url = USERS_MS_API_URL

# Create spei IN/OUT
def create_user(data):
    url = base_url + '/api/user'
    response = requests.post(url, json=data)
    return response.text
