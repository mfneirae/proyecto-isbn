import requests

from config import CATEGORIAS_MS_API_URL

base_url = CATEGORIAS_MS_API_URL

# Create category


def create_category(data):

    url = base_url

    response = requests.post(url, json=data)

    return response
