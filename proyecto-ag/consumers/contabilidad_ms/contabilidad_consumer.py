import requests

from config import CONTABILIDAD_MS_API_URL

base_url = CONTABILIDAD_MS_API_URL

# Create spei IN/OUT


def create_spei(data):

    url = base_url + '/api/user'

    response = requests.post(url, json=data)

    return response
