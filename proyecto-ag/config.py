import os

# Environment Variables (Microservices)

CONTABILIDAD_MS_API_URL = os.environ.get('CONTABILIDAD_MS_API_URL', 'http://localhost:8012')
CATEGORIAS_MS_API_URL = os.environ.get('CATEGORIAS_MS_API_URL','http://34.150.200.24:4000')
USERS_MS_API_URL = os.environ.get('USERS_MS_API_URL','https://users-cicd-mc-uizejnid7a-uk.a.run.app')
GOOGLE_CLIENT_ID = os.environ.get('GOOGLE_CLIENT_ID','826353873183-f1js2cb8832qnslcvm2ee8k0fkgqiddp.apps.googleusercontent.com')
GOOGLE_CLIENT_SECRET = os.environ.get('GOOGLE_CLIENT_SECRET','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')
DB_USER = os.environ.get('DB_USER','isbn')
DB_PASSWORD = os.environ.get('DB_PASSWORD','123')
DB_HOST = os.environ.get('DB_HOST','34.86.113.77')
DB_NAME = os.environ.get('DB_NAME','users-db')