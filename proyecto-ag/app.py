from ast import dump
import json
from flask import Flask, redirect, render_template, url_for, session
from flask_cors import CORS, cross_origin
from controllers.record_controller import *
from authlib.integrations.flask_client import OAuth
from config import GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, DB_USER, DB_PASSWORD, DB_HOST, DB_NAME
from Utils.SessionUser import SessionUser
from consumers.user_ms.user_consumer import *
from models.user import db
from services.user_service import  *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + '/' + DB_NAME 
db.init_app(app)
CORS(app)
app.secret_key = 'random_data'

## OAuth Config
oauth = OAuth(app)
google = oauth.register(
    name='google',
    client_id="826353873183-9g4o8si9vu9aqg966ajca25phu7k1mhs.apps.googleusercontent.com",
    client_secret="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",
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',  # This is only needed if using openId to fetch user info
    #client_kwargs={'scope': 'openid email profile'},
    client_kwargs={'scope': 'email profile'},
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration'
)

@app.route('/')
#@login_required
def hello_world():
    email = SessionUser.get_key_session_user('email')
    if email == "": 
        return redirect("/login")
    #return f'Hello, you are logge in as {email}!'
    return redirect("/index")


@cross_origin(origin='*',headers=['Content-Type','Authorization'])
@app.route('/index')
def index():
    if SessionUser.get_key_session_user('all') == "":
         return redirect('/login')
    return render_template('index.html')

@app.route('/login')
def login():
   oauth.create_client('google')
   redirect_uri = url_for('authorize', _external=True)
   return google.authorize_redirect(redirect_uri)

# @app.route('/login')
# def login():
#     return render_template('login.html')

@app.route('/authorize')
def authorize():
    oauth.create_client('google')
    token = google.authorize_access_token()
    resp = google.get('userinfo')
    user = oauth.google.userinfo()   
    #Create user
    #print("LOOKING FOR USER",UserService.get_user_service({ "email":user['email']}))
    userData = UserService.get_user_service({ "email":user['email']})
    if  userData == {} :
        data_response = create_user({"name":user['name'], "email":user['email']})
        response = json.loads(data_response)
    else:
        response = userData
    ##Save session
    SessionUser.create_session_user(user,'all')
    SessionUser.create_session_user(response["id"],'id_user')
    SessionUser.create_session_user(user['email'],'email')
    SessionUser.create_session_user(user['name'],'name')
    return redirect('/')

@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')

# Return session data
@record_api.route('/api/session/<data>', methods=['GET'])
def get_sessions(data):
    user = SessionUser.get_key_session_user(data)
    if  user == "":
        return jsonify(f"Unauthorized"), 400    
    return jsonify(user), 201

# @record_api.route('/user/<data>', methods=['GET'])
# def userget(data):
#     print("...",UserService.get_user_service({ "email":data}))
#     return jsonify(UserService.get_user_service({ "email":data}))

app.register_blueprint(record_api)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=4500)
