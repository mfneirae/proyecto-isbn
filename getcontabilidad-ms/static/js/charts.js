google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawCharts);

function drawCharts() {
    fetch('/api/spei/user/asd-123') // Asegúrate de reemplazar YOUR_USER_ID con el id de usuario apropiado.
    .then(response => response.json())
    .then(data => {
        // Función para calcular el total de una categoría
        function calculateTotal(dataObj) {
            let total = 0;
            Object.keys(dataObj).forEach(key => {
                total += dataObj[key];
            });
            return total;
        }

        // Gráfico de torta para Gastos
        var totalGastoByCategory = data.totals.total_gasto_by_category;
        var totalGasto = calculateTotal(totalGastoByCategory);
        var dataPieGasto = new google.visualization.DataTable();
        dataPieGasto.addColumn('string', 'Categoría');
        dataPieGasto.addColumn('number', 'Gasto');
        Object.keys(totalGastoByCategory).forEach(key => {
            dataPieGasto.addRow([key, totalGastoByCategory[key]]);
        });

        var optionsPieGasto = {
            title: 'Distribución del Gasto por Categoría',
            is3D: true,
            tooltip: {
                text: 'both'
            },
            pieSliceText: 'value-and-percentage'
        };

        var chartPieGasto = new google.visualization.PieChart(document.getElementById('piechart_gasto'));
        chartPieGasto.draw(dataPieGasto, optionsPieGasto);

        // Gráfico de torta para Ingresos
        var totalIngresoByCategory = data.totals.total_ingreso_by_category;
        var totalIngreso = calculateTotal(totalIngresoByCategory);
        var dataPieIngreso = new google.visualization.DataTable();
        dataPieIngreso.addColumn('string', 'Categoría');
        dataPieIngreso.addColumn('number', 'Ingreso');
        Object.keys(totalIngresoByCategory).forEach(key => {
            dataPieIngreso.addRow([key, totalIngresoByCategory[key]]);
        });

        var optionsPieIngreso = {
            title: 'Distribución del Ingreso por Categoría',
            is3D: true,
            tooltip: {
                text: 'both'
            },
            pieSliceText: 'value-and-percentage'
        };

        var chartPieIngreso = new google.visualization.PieChart(document.getElementById('piechart_ingreso'));
        chartPieIngreso.draw(dataPieIngreso, optionsPieIngreso);

        // Gráfico de barras
        var dataArrayBar = [['Categoría', 'Gasto', {role: 'annotation'}]];
        Object.keys(totalGastoByCategory).forEach(key => {
            dataArrayBar.push([key, totalGastoByCategory[key], totalGastoByCategory[key].toLocaleString('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 0})]);
        });
        dataArrayBar.sort((a, b) => b[1] - a[1]); // Ordenar de mayor a menor gasto

        var dataBar = google.visualization.arrayToDataTable(dataArrayBar);

        var optionsBar = {
            title: 'Gasto por Categoría',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Total Gastado',
                minValue: 0,
                format: '#,##0',  // Formato para eliminar decimales
                textStyle: {
                    fontSize: 12,
                    color: '#000',
                    auraColor: 'none'
                }
            },
            vAxis: {
                title: 'Categoría'
            },
            annotations: {
                textStyle: {
                    fontSize: 12,
                    color: '#000',
                    auraColor: 'none'
                },
                highContrast: true,  // Asegura que el texto de la anotación sea legible
                stem: { length: 0 }  // Elimina el tallo para que la anotación aparezca directamente sobre la barra
            },
            bar: { groupWidth: "95%" }  // Ajusta el ancho de las barras para dar espacio a las anotaciones
        };

        var chartBar = new google.visualization.BarChart(document.getElementById('barchart_values'));
        chartBar.draw(dataBar, optionsBar);


    })
    .catch(error => console.error('Error fetching data: ', error));
}
