from flask import Blueprint, request, jsonify
from services.spei_service import SpeiService

spei_api = Blueprint('spei_api', __name__)

@spei_api.route('/api/spei', methods=['POST'])
def create_spei_controller():
    data = request.get_json()
    SpeiService.create_spei_service(data)
    return jsonify("Spei has been successfully created."), 201

@spei_api.route('/api/spei/user/<id_user>', methods=['GET'])
def get_spei_by_user(id_user):
    result = SpeiService.get_spei_service_by_user(id_user)
    speis = [spei.to_dict() for spei in result['speis']]
    totals = result['totals']
    response = {
        "records": speis,
        "totals": totals
    }
    return jsonify(response), 200
