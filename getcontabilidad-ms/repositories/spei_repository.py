from models.spei import Spei, db
from sqlalchemy import func


class SpeiRepository:

    @staticmethod
    def create_spei_repository(id_user, id_category, type, amount):
        spei = Spei(id_user=id_user, id_category=id_category, type=type, amount=amount)
        db.session.add(spei)
        db.session.commit()
        return spei

    @staticmethod
    def get_spei_by_user(id_user):
        speis = Spei.query.filter_by(id_user=id_user).all()
        total_gasto = db.session.query(func.sum(Spei.amount)).filter(Spei.id_user == id_user, Spei.type == 'gasto').scalar()
        total_ingreso = db.session.query(func.sum(Spei.amount)).filter(Spei.id_user == id_user, Spei.type == 'ingreso').scalar()
        total_gasto_by_category = db.session.query(Spei.id_category, func.sum(Spei.amount)).filter(Spei.id_user == id_user, Spei.type == 'gasto').group_by(Spei.id_category).all()
        total_ingreso_by_category = db.session.query(Spei.id_category, func.sum(Spei.amount)).filter(Spei.id_user == id_user, Spei.type == 'ingreso').group_by(Spei.id_category).all()

        return speis, total_gasto, total_ingreso, total_gasto_by_category, total_ingreso_by_category
