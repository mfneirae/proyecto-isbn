from flask import Flask, render_template
from flask_cors import CORS
from models.spei import db
from controllers.spei_controller import spei_api
from config import DB_USER, DB_PASSWORD, DB_HOST, DB_NAME

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + '/' + DB_NAME 
CORS(app)
db.init_app(app)

# Registro del blueprint existente para la API
app.register_blueprint(spei_api)

# Nueva ruta para servir la página con gráficos
@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=4000)
