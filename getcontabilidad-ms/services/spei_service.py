from repositories.spei_repository import SpeiRepository

class SpeiService:

    @staticmethod
    def create_spei_service(data):
        return SpeiRepository.create_spei_repository(data['id_user'], data['id_category'], data['type'], data['amount'])

    @staticmethod
    def get_spei_service_by_user(id_user):
        speis, total_gasto, total_ingreso, total_gasto_by_category, total_ingreso_by_category = SpeiRepository.get_spei_by_user(id_user)
        return {
            "speis": speis,
            "totals": {
                "total_gasto": total_gasto,
                "total_ingreso": total_ingreso,
                "total_gasto_by_category": {cat: amount for cat, amount in total_gasto_by_category},
                "total_ingreso_by_category": {cat: amount for cat, amount in total_ingreso_by_category}
            }
        }
