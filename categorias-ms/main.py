import functions_framework

from google.cloud import firestore

db = firestore.Client(project="central-cinema-419712", database="categorias-db")

@functions_framework.http
def category_api(request):

    if request.method == 'POST':
        
        data = request.get_json()
    
        doc = db.collection("category").document(data['id'])
        doc.set(data['details'])

    return 'Category with id={} created!'.format(data['id']), 201
