from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Spei(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.String(100), nullable=False)
    id_category = db.Column(db.String(100), nullable=False)
    type = db.Column(db.String(100), nullable=False)
    amount = db.Column(db.Float, nullable=False)
