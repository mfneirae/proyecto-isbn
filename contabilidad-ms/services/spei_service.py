from repositories.spei_repository import SpeiRepository


class SpeiService:

    @staticmethod
    def create_spei_service(data):

        id_user = data['id_user']
        id_category = data['id_category']
        type = data['type']
        amount = data['amount']

        return SpeiRepository.create_spei_repository(id_user,
                                                     id_category,
                                                     type,
                                                     amount)
