from flask import Blueprint, request, jsonify
from services.spei_service import SpeiService

spei_api = Blueprint('spei_api', __name__)


@spei_api.route('/api/spei', methods=['POST'])
def create_spei_controller():

    data = request.get_json()
    SpeiService.create_spei_service(data)

    return jsonify("Spei has been successfully created."), 201
