from models.spei import Spei, db


class SpeiRepository:

    @staticmethod
    def create_spei_repository(id_user, id_category, type, amount):

        spei = Spei(id_user=id_user,
                    id_category=id_category,
                    type=type,
                    amount=amount)

        db.session.add(spei)
        db.session.commit()

        return spei
