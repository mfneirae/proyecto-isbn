import os

# Environment Variables (API Gateway)

#variable de entorno final
#ISBN_AG_API_URL = os.environ.get('ISBN_AG_API_URL')

#variable de entorno para pruebas (BD contabilidad)
ISBN_AG_API_URL = 'http://34.150.200.24:4000'
#variable de entorno para pruebas (BD categorias)
ISBN_NOSQL_API_URL = 'https://us-east4-central-cinema-419712.cloudfunctions.net/categoria-ms'