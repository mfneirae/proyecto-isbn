from flask import Blueprint, render_template, request, jsonify, redirect, url_for, flash
from services.record_service import RecordService
import webbrowser

record_api = Blueprint('record_api', __name__)

record_api.secret_key = 'mysecrectkey'

@record_api.route('/record', methods=['POST'])
def create_record():

    data = request.form
    user_name = data.get('user_name')
    user_email = data.get('user_email')
    task_id = data.get('task_id')
    task_name = data.get('task_name')
    task_description = data.get('task_description')

    if not user_name:
        return jsonify({'error': 'User name is required'}), 400

    if not task_name:
        return jsonify({'error': 'Task name is required'}), 400

    RecordService.create_record(user_name, user_email, task_id, task_name, task_description)
    #return redirect(url_for('record_api.index'))


@record_api.route('/')
def index():
    return render_template('index.html')


@record_api.route('/formulario_gasto')
def formulario_gasto():
    return render_template('crear_gasto.html')

@record_api.route('/crear_gasto', methods=['POST'])
def crear_gasto():
    data = request.form
    user_name = "5"
    type =  data.get('type')
    category_name = data.get('category_name')
    amount = data.get('amount')
    ## logica para traer id de usuario
    ## logica para consultar la categoria en BD firestore
    exist_category = existe_categoria(category_name)
    if(exist_category):
        flash("Registro creado exitosamente")
        RecordService.create_gasto(user_name, type, category_name, int(amount))
    else:
        flash("LA CATEGORIA NO EXISTE, DIRIJASE AL FORMULARIO DE CATEGORIA Y CREELA POR PRIMERA VEZ")
    print("PRUEBA: "+user_name+" "+type+" "+category_name+" "+amount)

    
    return redirect(url_for('record_api.index'))

@record_api.route('/ver_gastos')
def ver_gastos():
    webbrowser.open_new_tab('https://proyecto-ag-uizejnid7a-uk.a.run.app/')
    #return redirect('https://proyecto-ag-uizejnid7a-uk.a.run.app/')
    return redirect(url_for('record_api.index'))

@record_api.route('/formulario_categoria')
def formulario_categoria():
    return render_template('crear_categoria.html')


@record_api.route('/crear_categoria', methods=['POST'])
def crear_categoria():
    data = request.form
    user_name = "5"
    type =  data.get('type')
    category_name = data.get('category_name')
    description = data.get('description')
    exist_category = existe_categoria(category_name)
    if(exist_category):
        flash("La categoria ingresada ya existe")
    else:
        flash("Categoria creado exitosamente")
        RecordService.create_categoria(user_name, type, category_name, description)
    
    return redirect('/formulario_categoria')


def existe_categoria(categoria):
    categoria_response = RecordService.obtener_categoria(categoria)
    if categoria_response.status_code == 200:
        return True
    else:
        return False