import requests

from config import ISBN_AG_API_URL, ISBN_NOSQL_API_URL

base_url = ISBN_AG_API_URL
no_sql_url = ISBN_NOSQL_API_URL

# Create Record


def create_record(data):

    url = base_url + '/api/record'

    response = requests.post(url, json=data)

    return response


def create_gasto(data):

    url = base_url + '/api/spei'

    response = requests.post(url, json=data)
    return response

def create_categoria(data):

    url = no_sql_url
    response = requests.post(url, json=data)
    return response

def obtener_categoria(data):

    url = no_sql_url
    response = requests.get(url,params=data,)

    #category_json = response.json()
    #categ = category_json['categories'][0]['id_category']

    if response.status_code == 200:
        print("Success!")
    else:
        print("Not Found.")
    return response