from consumers.ag.record_consumer import *
import random

class RecordService:

    @staticmethod
    def create_record(user_name, user_email, task_id, task_name, task_description):

        data = {
            "user_name": user_name,
            "user_email": user_email,
            "task_id": task_id,
            "task_name": task_name,
            "task_description": task_description
        }

        return create_record(data)

    @staticmethod
    def create_gasto(id_user, type, id_category, amount):

        data = {
            "id_user": id_user,
            "id_category": id_category,
            "type": type,
            "amount": amount
        }
        print(data)
        return create_gasto(data)
    
    @staticmethod
    def obtener_categoria(category_name):
        data = {
            "category_name": category_name
        }
        print(data)
        return obtener_categoria(data)

    @staticmethod
    def create_categoria(user_name, type, category_name, description):
        id_category = random.randint(0, 1000000)
        id_category=str(id_category)
        data = {
           "id_category": id_category,
                "details": {
                "id_user": user_name,
                "category_name": category_name,
                "category_type": type,
                "description": description
            }
        }
        print(data)
        return create_categoria(data)